﻿using FluentValidation.Results;
using RenameWordStepsCalculation.Interfaces;
using RenameWordStepsCalculation.Models;
using RenameWordStepsCalculation.Services;
using RenameWordStepsCalculation.Validators; 
using Xunit;

namespace RenameWordStepsCalculation.Tests.Unit
{
    public class WordsShould
    {
        private readonly IWordCalculator _calculator;
        private readonly WordsValidator _validator;

        public WordsShould()
        {
            _calculator = new WordCalculator();
            //Arrange
            _validator = new WordsValidator();
        }

        [Fact]
        public void BeValid()
        {
            //Act
            Words sut = new Words() { FirstWord = "NotEmpty", SecondWord = "NotEmprty" };

            ValidationResult results = _validator.Validate(sut);

            //Assert
            Assert.True(results.IsValid);
        }

        [Theory]
        [InlineData("", "")]
        [InlineData("", "bla")]
        [InlineData("bla", "")]
        public void BeInvalid(string firstWord, string secondWord)
        {
            // Act
            Words sut = new Words()
            {
                FirstWord = firstWord,
                SecondWord = secondWord
            };

            ValidationResult results = _validator.Validate(sut);

            //Assert
            Assert.False(results.IsValid);
        }


        [Theory]
        [InlineData("cavalo", "gato", 4)]
        [InlineData("gato", "cavalo", 4)]
        [InlineData("pato", "gato", 1)]
        [InlineData("gato", "pato",1)]
        public void Calculate(string firstWord, string secondWord, int resultShould)
        {
            // Act
            Words sut = new Words()
            {
                FirstWord = firstWord,
                SecondWord = secondWord
            };

            var result = _calculator.CalculateStepsToRename(sut);

            //Assert
            Assert.Equal(resultShould, result);
        }
    }
}
