﻿using RenameWordStepsCalculation.Interfaces; 
using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using RenameWordStepsCalculation.Services;

namespace RenameWordStepsCalculation.Tests.Integration
{
    public abstract class IntegrationTestsBase
    {
        protected IWordCalculator _wordService;
        private TestServer _server;
        protected HttpClient _client; 

        protected IntegrationTestsBase()
        {
            //Arrange
            _server = new TestServer(new WebHostBuilder()
                .UseStartup<StartupTest>());
            _client = _server.CreateClient();
            _wordService = new WordCalculator();
         }
    }
}
