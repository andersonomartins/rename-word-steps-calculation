﻿using Newtonsoft.Json;
using RenameWordStepsCalculation.Models; 
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RenameWordStepsCalculation.Tests.Integration
{
    public class WordsShould : IntegrationTestsBase
    {

        [Theory]
        [InlineData("","")]
        [InlineData("","bla")]
        [InlineData("bla","")] 
        public async Task ReturnBadRequestWhenEmpty(string firstWord, string secondWord)
        {
            // Act
            Words sut = new Words()
            {
                FirstWord = firstWord,
                SecondWord = secondWord
            };

            StringContent content = new StringContent(JsonConvert.SerializeObject(sut),
             Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _client.PostAsync("/api/RenameWordsStepsCalc", content);
             //Assert 
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
