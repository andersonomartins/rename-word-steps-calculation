﻿using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RenameWordStepsCalculation.Interfaces;
using RenameWordStepsCalculation.Models;
using RenameWordStepsCalculation.Services;
using RenameWordStepsCalculation.Validators;

namespace RenameWordStepsCalculation.Tests
{
    public class StartupTest
    {
        public StartupTest(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddJsonOptions(options => { options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore; })
                .AddFluentValidation()  //Fluent Validation
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            //Services
            services.AddScoped<IWordCalculator, WordCalculator>();

            //Fluent Validation
            services.AddTransient<IValidator<Words>, WordsValidator>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMvc();
        }
    }
}
