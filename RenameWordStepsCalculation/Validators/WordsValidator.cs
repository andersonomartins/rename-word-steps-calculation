﻿using FluentValidation;
using RenameWordStepsCalculation.Models;

namespace RenameWordStepsCalculation.Validators
{
    public class WordsValidator : AbstractValidator<Words>
    {
        public WordsValidator()
        {
            RuleFor(x => x.FirstWord).NotEmpty();
            RuleFor(x => x.SecondWord).NotEmpty();
        }
    }
}
