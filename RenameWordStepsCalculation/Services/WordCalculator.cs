﻿using RenameWordStepsCalculation.Interfaces;
using RenameWordStepsCalculation.Models; 

namespace RenameWordStepsCalculation.Services
{
    public class WordCalculator : IWordCalculator
    {
        public int CalculateStepsToRename(Words words)
        {
            int count = 0;
            int appendCount = 0;
            if (words.FirstWord.Length < words.SecondWord.Length)
            {
                appendCount = words.SecondWord.Length - words.FirstWord.Length;
                words.SecondWord = words.SecondWord.Substring(appendCount);
            }
            else
            {
                appendCount = words.FirstWord.Length - words.SecondWord.Length;
                words.FirstWord = words.FirstWord.Substring(appendCount);
            }
             
            for (int i = 0; i < words.FirstWord.Length; i++)
            {
                if (words.FirstWord[i] != words.SecondWord[i])
                    count++;                
            }

            return count + appendCount;
        }
    }
}
