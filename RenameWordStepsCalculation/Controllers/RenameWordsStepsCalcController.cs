﻿ 
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RenameWordStepsCalculation.Interfaces;
using RenameWordStepsCalculation.Models;

namespace RenameWordStepsCalculation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RenameWordsStepsCalcController : ControllerBase
    {
        private readonly IWordCalculator _wordCalculator;

        public RenameWordsStepsCalcController(IWordCalculator wordCalculator)
        {
            _wordCalculator = wordCalculator;
        } 

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Words word)
        {
             return Ok(_wordCalculator.CalculateStepsToRename(word));
        }
    }
}
