﻿using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RenameWordStepsCalculation.Interfaces;
using RenameWordStepsCalculation.Middleware;
using RenameWordStepsCalculation.Models;
using RenameWordStepsCalculation.Services;
using RenameWordStepsCalculation.Validators;
using Swashbuckle.AspNetCore.Swagger;

namespace RenameWordStepsCalculation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddJsonOptions(options => { options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore; })
                .AddFluentValidation()  //Fluent Validation
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            //Services
            services.AddScoped<IWordCalculator, WordCalculator>();

            //Fluent Validation
            services.AddTransient<IValidator<Words>, WordsValidator>(); 

            //Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Rename Words Steps Calculations API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseExceptionMiddleware();// Todo remove
            }
            else
            {
                //app.UseExceptionMiddleware(); // Todo add
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Rename Words Steps Calculations API V1");
            });

            //Redirect swagger as initial page
            var option = new RewriteOptions();
            option.AddRedirect("^$", "swagger");
            app.UseRewriter(option);
        }
    }
}
