﻿using RenameWordStepsCalculation.Models; 

namespace RenameWordStepsCalculation.Interfaces
{
    public interface IWordCalculator
    {
        int CalculateStepsToRename(Words words);
    }
}
